<?php
/**
 * @file
 * callout.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function callout_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'edit any callout content'.
  $permissions['edit any callout content'] = array(
    'name' => 'edit any callout content',
    'roles' => array(
      'admin' => 'admin',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  return $permissions;
}
