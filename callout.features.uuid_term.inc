<?php
/**
 * @file
 * callout.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function callout_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => '4 Columns',
    'description' => '',
    'format' => 'rich_text',
    'weight' => 0,
    'uuid' => '1b597e42-f202-4cd6-8d8d-8bb13793aff3',
    'vocabulary_machine_name' => 'callout_size',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => '3 Columns',
    'description' => '',
    'format' => 'rich_text',
    'weight' => 0,
    'uuid' => '1b597e42-f202-4cd6-8d8d-8bb13793affe',
    'vocabulary_machine_name' => 'callout_size',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => '1 Column',
    'description' => '',
    'format' => 'rich_text',
    'weight' => 0,
    'uuid' => '82d12d8e-f833-4abf-8dd4-3e16d6176075',
    'vocabulary_machine_name' => 'callout_size',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => '2 Columns',
    'description' => '',
    'format' => 'rich_text',
    'weight' => 0,
    'uuid' => '949992df-fde0-46f5-b3c4-cd8358c8a2a3',
    'vocabulary_machine_name' => 'callout_size',
    'metatags' => array(),
  );
  return $terms;
}
